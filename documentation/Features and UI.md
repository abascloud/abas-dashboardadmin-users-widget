# abas-dashboardadmin-users-widget features and visible UI-components

The abas-dashboardadmin-users-widget can be used to display users and the groups they have been allocated.

## Images

![Alt text](Example.jpg "Widget base view")

## UI-Components

| Component | Description | Position
| --- | --- | --- |
| Filter users | Input box | Header
| Username | Displays the username and e-mail | Table
| Groups | Displays the groups allocated to the user | Table
| Edit-Button | Button | Table

## Features

| Component | Description | Input Type | 
| --- | --- | --- |
Filter users | Filters the table of users by name and email. | String | 
Edit-Button | Clicking the button opens the groups view for the user. | Button | 

## Groups View

![Alt text](ExampleGroups.jpg "Widget groups view")

| Title | Description | Input Type |
| --- | --- | --- | 
| Back Button | Applies the changes and returns to base view. | Button |
| Group Identifier | Displays full group name and id. | - |
| - | If checked the user is allocated to the group. | Checkbox |