console.log("Start generating locales.json");

const fs = require('fs');
const path = require('path');

const LOCALES_FILE = path.resolve("./locales.json");
const TMS_TRANS_PATH = path.resolve("./tms-trans-io/");
console.log("TMS_TRANS_PATH:" + TMS_TRANS_PATH);
console.log("LOCALES_FILE:" + LOCALES_FILE);

var localesJson = JSON.parse(fs.readFileSync(LOCALES_FILE, 'utf8'));

fs.readdirSync(TMS_TRANS_PATH)
  .filter((file) => file.endsWith('.json') && file !== 'locales.json')
  .forEach((file) => {
    console.log("processing " + file)
    const lang = path.basename(file, '.json');
    const langfile = path.resolve(TMS_TRANS_PATH, file);
    const newLocales = JSON.parse(fs.readFileSync(langfile, 'utf8'));
    localesJson[lang] = { ...localesJson[lang], ...newLocales }
  });

var prettyLocalesJson = JSON.stringify(localesJson, null, 4);
fs.writeFileSync(LOCALES_FILE, prettyLocalesJson, { encoding: 'utf8', flag: 'w' });

console.log("Done.")
